# Widget_Focus_And_Disabling.py

# imports
import tkinter as tk
from tkinter import ttk

# Create instance
win = tk.Tk()

# Add title
win.title("Python GUI")


# Modified Button Click Function
def click_me():
    action.configure(text="Hello " + name.get())


# Changing our Label
ttk.Label(win, text="Enter your name:").grid(column=0, row=0)

# Adding a Entry widget
name = tk.StringVar()
name_entry = ttk.Entry(win, width=12, textvariable=name)
name_entry.grid(column=0, row=1)

# Adding a Button
action = ttk.Button(win, text="Click Me!", command=click_me)
action.grid(column=1, row=1)

# Disable button widget
action.configure(state='disabled')

# Place cursor into name entry
name_entry.focus()

# Start GUI
win.mainloop()
