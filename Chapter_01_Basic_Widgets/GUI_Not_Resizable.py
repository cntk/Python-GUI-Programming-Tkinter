# GUI_Not_Resizable.py

# imports
import tkinter as tk

# Create instance
win = tk.Tk()

# Add title
win.title('Python GUI')

# Disable resizing the window by passing in False/False
win.resizable(False, False)

# Start GUI
win.mainloop()