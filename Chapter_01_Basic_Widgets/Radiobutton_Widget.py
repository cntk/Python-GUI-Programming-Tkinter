# Chapter_01_Basic_Widgets\Radiobutton_Widget.py

# imports
import tkinter as tk
from tkinter import ttk

# Create instance
win = tk.Tk()

# Add title
win.title("Python GUI")


# Modified Button Click Function
def click_me():
    action.configure(text="Hello " + name.get() + "  " + number_combobox.get())


# Changing our Label
ttk.Label(win, text="Enter your name:").grid(column=0, row=0)

# Adding a Entry widget
name = tk.StringVar()
name_entry = ttk.Entry(win, width=12, textvariable=name)
name_entry.grid(column=0, row=1)

# Adding a Button
action = ttk.Button(win, text="Click Me!", command=click_me)
action.grid(column=2, row=1)  # <= Change column to 2

ttk.Label(win, text="Choose a Number:").grid(column=1, row=0)
number = tk.StringVar()
number_combobox = ttk.Combobox(win, width=12, textvariable=number, state="readonly")
number_combobox["values"] = (1, 2, 4, 42, 100)
number_combobox.grid(column=1, row=1)  # <= Combobox in column 1
number_combobox.current(0)

# Create three checkbuttons
chVarDis = tk.IntVar()
check1 = tk.Checkbutton(win, text="Disabled", variable=chVarDis, state="disabled")
check1.select()
check1.grid(column=0, row=4, sticky=tk.W)

chVarUn = tk.IntVar()
check2 = tk.Checkbutton(win, text="UnChecked", variable=chVarUn)
check2.deselect()
check2.grid(column=1, row=4, sticky=tk.W)

chVarEn = tk.IntVar()
check3 = tk.Checkbutton(win, text="Enabled", variable=chVarEn)
check3.select()
check3.grid(column=2, row=4, sticky=tk.W)

# Radiobutton Globals
COLOR_BLUE = "Blue"
COLOR_GOLD = "Gold"
COLOR_RED = "Red"


# Radiobutton Callback
def radFunc():
    color_sec = radVar.get()
    if color_sec == 1:
        win.configure(background=COLOR_BLUE)
    elif color_sec == 2:
        win.configure(background=COLOR_GOLD)
    elif color_sec == 3:
        win.configure(background=COLOR_RED)


radVar = tk.IntVar()

rad1 = tk.Radiobutton(win, text=COLOR_BLUE, variable=radVar, value=1, command=radFunc)
rad1.grid(column=0, row=5, sticky=tk.W)
rad2 = tk.Radiobutton(win, text=COLOR_GOLD, variable=radVar, value=2, command=radFunc)
rad2.grid(column=1, row=5, sticky=tk.W)
rad3 = tk.Radiobutton(win, text=COLOR_RED, variable=radVar, value=3, command=radFunc)
rad3.grid(column=2, row=5, sticky=tk.W)

# Place cursor into name entry
name_entry.focus()

# Start GUI
win.mainloop()
